package org.sokirka.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.stream.Stream;

public class Parser {

    public static Stream<String> parseFile(String uri) {
        Stream<String> stringStream = Stream.empty();
        if (Objects.isNull(uri) || uri.isEmpty()) {
            return Stream.empty();
        }

        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File(uri)));
            stringStream = reader.lines();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringStream;
    }
}
