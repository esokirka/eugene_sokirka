package org.sokirka.strategy;

public enum  Strategy {
    RANDOM("rand"),
    DOWN_SEQ("downseq"),
    UP_SEQ("upseq");

    private String value;

    Strategy(String value) {
        this.value = value;
    }

    public static Strategy valueOfString(String value) {
        for (Strategy v : Strategy.values()) {
            if (v.value.equalsIgnoreCase(value)) {
                return v;
            }
        }
        throw new IllegalArgumentException("Illegal enum argument");
    }


}
