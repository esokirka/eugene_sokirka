package org.sokirka.context;

import org.sokirka.strategy.Strategy;

import java.util.Objects;

public class AppContext {

    private Strategy appStrategy;
    private static AppContext instance;

    private AppContext() {
        this.appStrategy = Strategy.RANDOM;
    }

    public static AppContext getInstance() {
        if (Objects.isNull(instance)) {
            instance = new AppContext();
        }
        return instance;
    }

    public Strategy getAppStrategy() {
        return appStrategy;
    }

    public void setAppStrategy(Strategy appStrategy) {
        this.appStrategy = appStrategy;
    }
}
