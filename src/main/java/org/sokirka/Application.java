package org.sokirka;

import org.sokirka.context.AppContext;
import org.sokirka.parser.Parser;
import org.sokirka.service.AnswerService;
import org.sokirka.strategy.Strategy;
import org.sokirka.vocabulary.VocabularyStorage;

import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Stream;

public class Application {

    private AnswerService answerService;

    public static void main(String[] args) {
        Application application = new Application();
        application.initApp(args[0], args[1]);

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {

            String request = scanner.next();
            String response;

            if ("calculate:".equalsIgnoreCase(request)) {
                response = String.valueOf(application.calculate(request.split(":")[1]));
            } else if ("strategy:".equalsIgnoreCase(request)) {
                application.changeStrategy(request.split(":")[1]);
                response = "Done";
            } else {
                response = application.getAnswerByStrategy();
            }

            System.out.println(response);
        }
    }

    public Application() {
    }

    public void initApp(String strategy, String file) {
        if (Objects.isNull(strategy)) {
            AppContext.getInstance();
        } else {
            AppContext.getInstance().setAppStrategy(Strategy.valueOfString(strategy));
        }

        Stream<String> stringStream = Parser.parseFile(file);
        VocabularyStorage storage = new VocabularyStorage();
        storage.addPhrases(stringStream);
        answerService = new AnswerService(storage);
    }

    public String getAnswerByStrategy() {
        return answerService.getAnswer();
    }

    public int calculate(String inputValue) {
        if (!inputValue.contains("+")) {
            return -1;
        }
        String[] split = inputValue.trim().split("+");
        return Integer.parseInt(split[0]) + Integer.parseInt(split[1]);
    }

    public void changeStrategy(String strategyValue) {
        Strategy strategy = Strategy.valueOfString(strategyValue);
        AppContext.getInstance().setAppStrategy(strategy);
    }
}
