package org.sokirka.vocabulary;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class VocabularyStorage {

    private final static Set<String> storage = new HashSet<>();

    public void addPhrases(Stream<String> phrases) {
        phrases.forEach(storage::add);
        phrases.close();
    }

    public Set<String> getVocabulary() {
        return storage;
    }
}
